JioCema Changelog
===================

## WIP

## v1.101.12
- Update Jacoco & SoftBugs for JDK23
- Update Maven plugins
- Update PMD & Checkstyle

## v1.101.10
Released on 2023-10-10
- JUnit4 to JUnit5
- update Maven plugins for JDK20

## v1.101.8
Released on 2022-10-07
- split test case because JDK13 and Unicode corrigendum#9
- Add explicit modifier to interface for JDK9

## v1.101.6
Released on 2021-05-16
- responding to junit security issue GHSA-269g-pwp5-87pp

## v1.101.4
Released on 2019-03-21
- responding to checkstyle security issue CVE-2019-9658

## v1.101.2
Released on 2018-03-01
- Initial Release

--- EOF ---
