/*
 * License : The MIT License
 * Copyright(c) 2018 olyutorskii
 */

package io.bitbucket.olyutorskii.jiocema;

/**
 *
 */
class TestListenerRW extends TestListener{

    @Override
    public void rawBytes(byte[] byteArray, int offset, int length)
            throws DecodeBreakException {
        append("[RW]");
        dumpHex(byteArray, offset, length);
        return;
    }

}
