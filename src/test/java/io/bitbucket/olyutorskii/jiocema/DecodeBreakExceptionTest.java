/*
 * License : The MIT License
 * Copyright(c) 2018 olyutorskii
 */

package io.bitbucket.olyutorskii.jiocema;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 *
 */
public class DecodeBreakExceptionTest {

    public DecodeBreakExceptionTest() {
    }

    @Test
    public void testSomeMethod() {

        Throwable th;
        Throwable cause;

        cause = new Throwable("PQR");

        th = new DecodeBreakException();
        assertNull(th.getMessage());
        assertNull(th.getCause());

        th = new DecodeBreakException("ABC");
        assertEquals("ABC", th.getMessage());
        assertNull(th.getCause());

        th = new DecodeBreakException(cause);
        assertEquals(cause.toString(), th.getMessage());
        assertEquals(cause, th.getCause());

        th = new DecodeBreakException("XYZ", cause);
        assertEquals("XYZ", th.getMessage());
        assertEquals(cause, th.getCause());

        return;
    }

}
