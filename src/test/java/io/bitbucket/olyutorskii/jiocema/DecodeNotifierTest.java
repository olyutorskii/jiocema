/*
 * License : The MIT License
 * Copyright(c) 2018 olyutorskii
 */

package io.bitbucket.olyutorskii.jiocema;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.MalformedInputException;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 *
 */
public class DecodeNotifierTest {

    static final Charset CS_ASCII    = Charset.forName("US-ASCII");
    static final Charset CS_UTF8     = Charset.forName("UTF-8");
    static final Charset CS_UTF16BE  = Charset.forName("UTF-16BE");
    static final Charset CS_UTF16LE  = Charset.forName("UTF-16LE");
    static final Charset CS_UTF16    = Charset.forName("UTF-16");
    static final Charset CS_ISO2022  = Charset.forName("ISO-2022-JP");

    static final boolean CORRIGENDUM9 = corrigendum9();


    public DecodeNotifierTest() {
    }


    static ByteArrayInputStream byteStream(int... array){
        byte[] ba = new byte[array.length];

        int idx = 0;
        for(int iVal : array){
            byte bVal = (byte)(iVal & 0xff);
            ba[idx++] = bVal;
        }

        return new ByteArrayInputStream(ba);
    }

    /**
     * Corrigendum #9 checker.
     *
     * Noncharacters(like reverse-BOM U+FFFE) must be normaly decoded
     * if Unicode 7 decoder.
     *
     * It may be true if JDK13 or later.
     *
     * @return true if JavaNIO decoder supported Corrigendum #9
     *
     * @see JDK-8216140 : Correct UnicodeDecoder U+FFFE handling
     * @see Unicode Corrigendum #9: Clarification About Noncharacters
     */
    static boolean corrigendum9() {
        char rvbom = '\ufffe';
        byte[] ba_rvbom = {(byte) 0xff, (byte) 0xfe};

        Charset cs;
        CharsetDecoder cd;
        ByteBuffer bb;
        CharBuffer cb;

        cs = StandardCharsets.UTF_16BE;

        cd = cs.newDecoder();
        cd.onMalformedInput(CodingErrorAction.REPORT);

        bb = ByteBuffer.allocate(10);
        bb.put(ba_rvbom);
        bb.flip();

        try {
            cb = cd.decode(bb);
        } catch (MalformedInputException e) {
            return false;
        } catch (CharacterCodingException e) {
            return false;
        }

        if (cb.get() != rvbom) {
            return false;
        }

        return true;
    }

    /**
     * Test of decode method, of class DecodeNotifier.
     * @throws java.lang.Exception
     */
    @Test
    public void testDecode() throws Exception {
        System.out.println("decode");

        CharsetDecoder cd;
        DecodeNotifier decoder;
        TestListener lst;
        InputStream is;
        ByteBuffer bBuf;
        CharBuffer cBuf;

        cd = CS_ASCII.newDecoder();


        // test constructor error

        decoder = null;
        try{
            decoder = new DecodeNotifier(null);
            fail();
        }catch(NullPointerException e){
        }
        assert decoder == decoder;

        try{
            decoder = new DecodeNotifier(cd, 0, 100);
            fail();
        }catch(IllegalArgumentException e){
            assertEquals("input buffer must have an effective length", e.getMessage());
        }

        try{
            decoder = new DecodeNotifier(cd, 100, 0);
            fail();
        }catch(IllegalArgumentException e){
            assertEquals("output buffer length must be 2 or more for surrogate pair", e.getMessage());
        }

        assert decoder == decoder;


        // test default buf-size

        decoder = new DecodeNotifier(cd);
        bBuf = decoder.getByteBuffer();
        cBuf = decoder.getCharBuffer();
        assert bBuf.capacity() == DecodeNotifier.DEFSZ_BYTEBUF;
        assert cBuf.capacity() == DecodeNotifier.DEFSZ_CHARBUF;


        // test decoding

        decoder = new DecodeNotifier(cd, 4, 4);

        lst = new TestListener();
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream();
        decoder.decode(is);
        assertEquals("[ST][EN]", lst.toString());

        lst.clear();
        is = byteStream(0x41);
        decoder.decode(is);
        assertEquals("[ST][CH]A[EN]", lst.toString());

        lst.clear();
        is = byteStream(0x41, 0x42, 0x43, 0x44);
        decoder.decode(is);
        assertEquals("[ST][CH]ABCD[EN]", lst.toString());

        lst.clear();
        is = byteStream(0x41, 0x42, 0x43, 0x44, 0x45);
        decoder.decode(is);
        assertEquals("[ST][CH]ABCD[CH]E[EN]", lst.toString());


        // test no listener

        lst.clear();
        decoder.setCharDecodeListener(null);
        is = byteStream(0x41, 0x80);
        decoder.decode(is);
        assertEquals("", lst.toString());

        Charset cs;
        cs = Charset.forName("x-JIS0208");
        cd = cs.newDecoder();
        decoder = new DecodeNotifier(cd);
        is = byteStream(0x30, 0x20);  // Unmapped in JIS X 0208
        decoder.decode(is);
        assertEquals("", lst.toString());

        return;
    }

    /**
     * Test of US_ASCII decoding, of class DecodeNotifier.
     * @see sun.nio.cs.US_ASCII
     * @throws java.lang.Exception
     */
    @Test
    public void testUS_ASCII_Decode() throws Exception {
        System.out.println("ASCII");

        CharsetDecoder cd;
        DecodeNotifier decoder;
        TestListener lst;
        InputStream is;

        // test 7bit-US_ASCII decoding

        cd = CS_ASCII.newDecoder();
        decoder = new DecodeNotifier(cd);

        lst = new TestListener();
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x00, 0x1f, 0x20, 0x30, 0x41, 0x61, 0x7e, 0x7f);
        decoder.decode(is);
        assertEquals("[ST][CH]\u0000\u001f\u00200Aa~\u007f[EN]", lst.toString());

        lst.clear();
        is = byteStream(0x23, 0x24, 0x40, 0x5b, 0x5d, 0x5e, 0x60, 0x7b, 0x7c, 0x7d);
        decoder.decode(is);
        assertEquals("[ST][CH]#$@[]^`{|}[EN]", lst.toString());

        lst.clear();
        is = byteStream(0x41, 0x5c, 0x42);
        decoder.decode(is);
        assertEquals("[ST][CH]A\u005c\u005cB[EN]", lst.toString());

        // test 8bit-code decoding (illegal)

        lst.clear();
        is = byteStream(0x80);
        decoder.decode(is);
        assertEquals("[ST][ME]80[EN]", lst.toString());

        lst.clear();
        is = byteStream(0xff);
        decoder.decode(is);
        assertEquals("[ST][ME]ff[EN]", lst.toString());

        lst.clear();
        is = byteStream(0x41, 0x80, 0x42);
        decoder.decode(is);
        assertEquals("[ST][CH]A[ME]80[CH]B[EN]", lst.toString());

        lst.clear();
        is = byteStream(0x80, 0x80);
        decoder.decode(is);
        assertEquals("[ST][ME]80[ME]80[EN]", lst.toString());

        return;
    }

    /**
     * Test of UTF16 decoding, of class DecodeNotifier.
     * @see sun.nio.cs.UnicodeDecoder
     * @throws java.lang.Exception
     */
    @Test
    public void testUTF_16_BE() throws Exception {
        System.out.println("UTF16");

        CharsetDecoder cd;
        DecodeNotifier decoder;
        TestListener lst;
        InputStream is;

        // test UTF16BE decoding

        cd = CS_UTF16BE.newDecoder();
        decoder = new DecodeNotifier(cd);

        lst = new TestListener();
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x00, 0x41);
        decoder.decode(is);
        assertEquals("[ST][CH]A[EN]", lst.toString());

        lst.clear();
        is = byteStream(0x00, 0x80);
        decoder.decode(is);
        assertEquals("[ST][CH]\u0080[EN]", lst.toString());

        lst.clear();
        is = byteStream(0x9f, 0xd5);
        decoder.decode(is);
        assertEquals("[ST][CH]\u9fd5[EN]", lst.toString());

        lst.clear();
        is = byteStream(0xff, 0xfd);
        decoder.decode(is);
        assertEquals("[ST][CH]\ufffd[EN]", lst.toString());

        lst.clear();
        is = byteStream(0xff, 0xff);
        decoder.decode(is);
        assertEquals("[ST][CH]\uffff[EN]", lst.toString());

        return;
    }

    /**
     * Test of decode method, of class DecodeNotifier.
     * @see sun.nio.cs.UnicodeDecoder
     * @throws java.lang.Exception
     */
    @Test
    public void testUTF_8() throws Exception {
        System.out.println("decode");

        CharsetDecoder cd;
        DecodeNotifier decoder;
        TestListener lst;
        InputStream is;

        cd = CS_UTF8.newDecoder();
        decoder = new DecodeNotifier(cd, 100, 100);

        lst = new TestListener();
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x41);
        decoder.decode(is);
        assertEquals("[ST][CH]A[EN]", lst.toString());

        lst.clear();
        is = byteStream(0xc2, 0xa5);
        decoder.decode(is);
        assertEquals("[ST][CH]\u00a5[EN]", lst.toString());

        lst.clear();
        is = byteStream(0xe2, 0x91, 0xa0);
        decoder.decode(is);
        assertEquals("[ST][CH]\u2460[EN]", lst.toString());

        lst.clear();
        is = byteStream(0xf0, 0x90, 0x8c, 0x80);
        decoder.decode(is);
        assertEquals("[ST][CH]\ud800\udf00[EN]", lst.toString());

        assertTrue(0x10FFFF <= Character.MAX_CODE_POINT);
        assertTrue(0x200000 > Character.MAX_CODE_POINT);
        lst.clear();
        is = byteStream(0xf0, 0x90, 0xbf, 0xbf);
        decoder.decode(is);
        assertEquals("[ST][CH]\ud803\udfff[EN]", lst.toString());

        // CESU-8  U+D800, U+DF00
        lst.clear();
        is = byteStream(0xef, 0xbf, 0xbd, 0xef, 0xbf, 0xbd);
        decoder.decode(is);
        assertEquals("[ST][CH]\ufffd\ufffd[EN]", lst.toString());

        return;
    }

    /**
     * Test of infinite loop on decoding, of class DecodeNotifier.
     * @see sun.nio.cs.UnicodeDecoder
     * @throws java.lang.Exception
     */
    @Test
    public void testInfLoop() throws Exception {
        System.out.println("infiniteLoop");

        CharsetDecoder cd;
        DecodeNotifier decoder;
        TestListener lst;
        InputStream is;


        // test decoding with enough length input buffer

        cd = CS_UTF16.newDecoder();
        decoder = new DecodeNotifier(cd, 2, 100);

        lst = new TestListener();
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x00, 0x41);
        decoder.decode(is);
        assertEquals("[ST][CH]A[EN]", lst.toString());


        // test detecting infinite loop

        cd = CS_UTF16.newDecoder();
        decoder = new DecodeNotifier(cd, 1, 100);
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x00, 0x41);
        try{
            decoder.decode(is);
            fail();
        }catch(IllegalStateException e){
            assertEquals("too small input buffer (1bytes) for UTF-16", e.getMessage());
        }

        return;
    }

    /**
     * Test of decode buffering, of class DecodeNotifier.
     * @see sun.nio.cs.UnicodeDecoder
     * @throws java.lang.Exception
     */
    @Test
    public void testBuffering() throws Exception {
        System.out.println("buffering");

        CharsetDecoder cd;
        DecodeNotifier decoder;
        TestListener lst;
        InputStream is;


        // test input buffering

        cd = CS_UTF16.newDecoder();
        decoder = new DecodeNotifier(cd, 5, 100);

        lst = new TestListenerRW();
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x00, 0x41, 0x00, 0x42, 0x00, 0x43);
        decoder.decode(is);
        assertEquals("[ST][RW]00410042[RW]0043[CH]ABC[EN]", lst.toString());


        // test output buffering

        decoder = new DecodeNotifier(cd, 100, 2);
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x00, 0x41, 0x00, 0x42, 0x00, 0x43);
        decoder.decode(is);
        assertEquals("[ST][RW]00410042[CH]AB[RW]0043[CH]C[EN]", lst.toString());


        // test input&output buffering

        decoder = new DecodeNotifier(cd, 3, 2);
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x00, 0x41, 0x00, 0x42, 0x00, 0x43);
        decoder.decode(is);
        assertEquals("[ST][RW]0041[RW]0042[CH]AB[RW]0043[CH]C[EN]", lst.toString());

        return;
    }

    /**
     * Test of BOM decoding, of class DecodeNotifier.
     * @see sun.nio.cs.UnicodeDecoder
     * @throws java.lang.Exception
     */
    @Test
    public void testUTFBOM() throws Exception {
        System.out.println("BOM!");

        CharsetDecoder cd;
        DecodeNotifier decoder;
        TestListener lst;
        InputStream is;


        // test BOM with BE decoder

        cd = CS_UTF16BE.newDecoder();
        decoder = new DecodeNotifier(cd);

        lst = new TestListener();
        decoder.setCharDecodeListener(lst);

        // test BOM BE leading
        lst.clear();
        is = byteStream(0xfe, 0xff, 0x00, 0x41);
        decoder.decode(is);
        assertEquals("[ST][CH]\ufeffA[EN]", lst.toString());

        // test BOM BE trailing
        lst.clear();
        is = byteStream(0x00, 0x41, 0xfe, 0xff);
        decoder.decode(is);
        assertEquals("[ST][CH]A\ufeff[EN]", lst.toString());

        // test BOM LE (illegal)leading
        lst.clear();
        is = byteStream(0xff, 0xfe, 0x00, 0x41);
        decoder.decode(is);
        if (CORRIGENDUM9) {
            assertEquals("[ST][CH]\ufffeA[EN]", lst.toString());

        } else {
            assertEquals("[ST][ME]fffe[CH]A[EN]", lst.toString());
        }

        // test BOM LE (illegal)trailing
        lst.clear();
        is = byteStream(0x00, 0x41, 0xff, 0xfe);
        decoder.decode(is);
        if (CORRIGENDUM9) {
            assertEquals("[ST][CH]A\ufffe[EN]", lst.toString());

        } else {
            assertEquals("[ST][CH]A[ME]fffe[EN]", lst.toString());
        }

        // test BOM with LE decoder

        cd = CS_UTF16LE.newDecoder();
        decoder = new DecodeNotifier(cd);

        lst = new TestListener();
        decoder.setCharDecodeListener(lst);

        // test BOM LE leading
        lst.clear();
        is = byteStream(0xff, 0xfe, 0x41, 0x00);
        decoder.decode(is);
        assertEquals("[ST][CH]\ufeffA[EN]", lst.toString());

        // test BOM LE trailing
        lst.clear();
        is = byteStream(0x41, 0x00, 0xff, 0xfe);
        decoder.decode(is);
        assertEquals("[ST][CH]A\ufeff[EN]", lst.toString());

        // test BOM BE (illegal)leading
        lst.clear();
        is = byteStream(0xfe, 0xff, 0x41, 0x00);
        decoder.decode(is);
        if (CORRIGENDUM9) {
            assertEquals("[ST][CH]\ufffeA[EN]", lst.toString());
        } else {
            assertEquals("[ST][ME]feff[CH]A[EN]", lst.toString());
        }

        // test BOM BE (illegal)trailing
        lst.clear();
        is = byteStream(0x41, 0x00, 0xfe, 0xff);
        decoder.decode(is);
        if (CORRIGENDUM9) {
            assertEquals("[ST][CH]A\ufffe[EN]", lst.toString());
        } else {
            assertEquals("[ST][CH]A[ME]feff[EN]", lst.toString());
        }


        // test BOM with auto-detect-byte-order decoder

        cd = CS_UTF16.newDecoder();
        decoder = new DecodeNotifier(cd);

        lst = new TestListener();
        decoder.setCharDecodeListener(lst);

        // test BOM BE leading
        lst.clear();
        is = byteStream(0xfe, 0xff, 0x00, 0x41);
        decoder.decode(is);
        assertEquals("[ST][CH]A[EN]", lst.toString());

        // test BOM BE trailing
        lst.clear();
        is = byteStream(0x00, 0x41, 0xfe, 0xff);
        decoder.decode(is);
        assertEquals("[ST][CH]A\ufeff[EN]", lst.toString());

        // test BOM BE leading & trailing
        lst.clear();
        is = byteStream(0xfe, 0xff, 0x00, 0x41, 0xfe, 0xff);
        decoder.decode(is);
        assertEquals("[ST][CH]A\ufeff[EN]", lst.toString());

        // test BOM BE leading & (illegal)trailing
        lst.clear();
        is = byteStream(0xfe, 0xff, 0x00, 0x41, 0xff, 0xfe);
        decoder.decode(is);
        if (CORRIGENDUM9) {
            assertEquals("[ST][CH]A\ufffe[EN]", lst.toString());
        } else {
            assertEquals("[ST][CH]A[ME]fffe[EN]", lst.toString());
        }

        // test BOM LE leading
        lst.clear();
        is = byteStream(0xff, 0xfe, 0x41, 0x00);
        decoder.decode(is);
        assertEquals("[ST][CH]A[EN]", lst.toString());

        // test BOM LE trailing (detection failed)
        lst.clear();
        is = byteStream(0x41, 0x00, 0xff, 0xfe);
        decoder.decode(is);
        if (CORRIGENDUM9) {
            assertEquals("[ST][CH]\u4100\ufffe[EN]", lst.toString());
        } else {
            assertEquals("[ST][CH]\u4100[ME]fffe[EN]", lst.toString());
        }

        // test BOM LE leading & trailing
        lst.clear();
        is = byteStream(0xff, 0xfe, 0x41, 0x00, 0xff, 0xfe);
        decoder.decode(is);
        assertEquals("[ST][CH]A\ufeff[EN]", lst.toString());

        // test BOM LE leading & (illegal)trailing
        lst.clear();
        is = byteStream(0xff, 0xfe, 0x41, 0x00, 0xfe, 0xff);
        decoder.decode(is);
        if (CORRIGENDUM9) {
            assertEquals("[ST][CH]A\ufffe[EN]", lst.toString());
        } else {
            assertEquals("[ST][CH]A[ME]feff[EN]", lst.toString());
        }

        return;
    }

    /**
     * Test of locking shift encoding, of class DecodeNotifier.
     * @see sun.nio.cs.ext.ISO2022_JP
     * @throws java.lang.Exception
     */
    @Test
    public void testLockingShift() throws Exception {
        System.out.println("locking-shift");

        CharsetDecoder cd;
        DecodeNotifier decoder;
        TestListener lst;
        InputStream is;

        cd = CS_ISO2022.newDecoder();
        decoder = new DecodeNotifier(cd);

        lst = new TestListener();
        decoder.setCharDecodeListener(lst);

        // test locking-shift

        lst.clear();
        is = byteStream(
                0x41,
                0x1b, 0x24, 0x42,
                0x30, 0x21,             // '亜' U+4E9C
                0x1b, 0x28, 0x42,
                0x43 );
        decoder.decode(is);
        assertEquals("[ST][CH]A\u4e9cC[EN]", lst.toString());

        // test locking-shift not recovered

        lst.clear();
        is = byteStream(
                0x41,
                0x1b, 0x24, 0x42,
                0x30, 0x21 );
        decoder.decode(is);
        assertEquals("[ST][CH]A\u4e9c[EN]", lst.toString());

        // test lacking locking-shift designate

        lst.clear();
        is = byteStream(
                0x41,
                0x1b, 0x24, 0x42,
                0x30, 0x21,
                0x1b );
        decoder.decode(is);
        assertEquals("[ST][CH]A\u4e9c[ME]1b[EN]", lst.toString());

        // test illegal locking-shift designate

        lst.clear();
        is = byteStream(
                0x41,
                0x1b, 0x24, 0xff,
                0x30, 0x21,
                0x1b, 0x28, 0x42,
                0x43 );
        decoder.decode(is);
        assertEquals("[ST][CH]A[ME]1b24ff[CH]0!C[EN]", lst.toString());

        return;
    }

    /**
     * Test of unmap-error, of class DecodeNotifier.
     * @see sun.nio.cs.ext.JIS_X_0208_Decoder
     * @throws java.lang.Exception
     */
    @Test
    public void testUnmap() throws Exception {
        System.out.println("unmap-error");

        Charset cs;
        CharsetDecoder cd;
        DecodeNotifier decoder;
        TestListener lst;
        InputStream is;

        // test unmap error

        cs = Charset.forName("x-JIS0208");
        assertNotNull(cs);
        cd = cs.newDecoder();
        decoder = new DecodeNotifier(cd);

        lst = new TestListener();
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x30, 0x21);  // '亜' U+4E9C
        decoder.decode(is);
        assertEquals("[ST][CH]\u4e9c[EN]", lst.toString());

        lst.clear();
        is = byteStream(0x30, 0x20);  // Unmapped in JIS X 0208
        decoder.decode(is);
        assertEquals("[ST][UE]3020[EN]", lst.toString());

        // test odd length sequence

        lst.clear();
        is = byteStream(0x30, 0x21, 0x31);
        decoder.decode(is);
        assertEquals("[ST][CH]\u4e9c[ME]31[EN]", lst.toString());

        return;
    }

    /**
     * Test of surrogate-pair decoding, of class DecodeNotifier.
     * @see sun.nio.cs.UnicodeDecoder
     * @throws java.lang.Exception
     */
    @Test
    public void testSurrogatePair() throws Exception {
        System.out.println("surrogate-pair");

        CharsetDecoder cd;
        DecodeNotifier decoder;
        TestListener lst;
        InputStream is;

        cd = CS_UTF16BE.newDecoder();
        decoder = new DecodeNotifier(cd);

        lst = new TestListener();
        decoder.setCharDecodeListener(lst);

        // test surrogate pair

        lst.clear();
        is = byteStream(0xd8, 0x40, 0xdc, 0x00);
        decoder.decode(is);
        assertEquals("[ST][CH]\ud840\udc00[EN]", lst.toString());

        // test surrogate-pair with lacked Lo-char

        lst.clear();
        is = byteStream(0xd8, 0x40, 0xdc);
        decoder.decode(is);
        assertEquals("[ST][ME]d840dc[EN]", lst.toString());

        // test surrogate-pair without Lo-char

        lst.clear();
        is = byteStream(0xd8, 0x40);
        decoder.decode(is);
        assertEquals("[ST][ME]d840[EN]", lst.toString());

        // test surrogate-pair with non Lo-char

        lst.clear();
        is = byteStream(0xd8, 0x40, 0x00, 0x41, 0x00, 0x42);
        decoder.decode(is);
        assertEquals("[ST][ME]d8400041[CH]B[EN]", lst.toString());

        // test surrogate-pair with non Hi-char

        lst.clear();
        is = byteStream(0xdc, 0x00);
        decoder.decode(is);
        assertEquals("[ST][ME]dc00[EN]", lst.toString());

        // test error recovery with surrogate-pair without Hi-char

        lst.clear();
        is = byteStream(0xdc, 0x00, 0x00, 0x41);
        decoder.decode(is);
        assertEquals("[ST][ME]dc00[CH]A[EN]", lst.toString());

        // test surrogate-pair with lacked Hi-char

        lst.clear();
        is = byteStream(0xd8);
        decoder.decode(is);
        assertEquals("[ST][ME]d8[EN]", lst.toString());

        // test surrogate-pair but 1-char buffer space left only

        cd = CS_UTF16BE.newDecoder();
        decoder = new DecodeNotifier(cd);
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x00, 0x41, 0xd8, 0x40, 0xdc, 0x00);
        decoder.decode(is);
        assertEquals("[ST][CH]A\ud840\udc00[EN]", lst.toString());

        cd = CS_UTF16BE.newDecoder();
        decoder = new DecodeNotifier(cd, 100, 2);
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x00, 0x41, 0xd8, 0x40, 0xdc, 0x00);
        decoder.decode(is);
        assertEquals("[ST][CH]A[CH]\ud840\udc00[EN]", lst.toString());

        return;
    }

    /**
     * Test of rawBytes method, of class CharDecodeListener.
     * @throws java.lang.Exception
     */
    @Test
    public void testRawNotify() throws Exception {
        System.out.println("rawBytes");

        Charset cs;
        CharsetDecoder cd;
        DecodeNotifier decoder;
        TestListener lst;
        InputStream is;

        // test that raw notification is split caused by small input buffer

        cd = CS_ASCII.newDecoder();
        decoder = new DecodeNotifier(cd, 3, 100);

        lst = new TestListenerRW();
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x41, 0x42, 0x43);
        decoder.decode(is);
        assertEquals("[ST][RW]414243[CH]ABC[EN]", lst.toString());

        lst.clear();
        is = byteStream(0x41, 0x42, 0x43, 0x44);
        decoder.decode(is);
        assertEquals("[ST][RW]414243[RW]44[CH]ABCD[EN]", lst.toString());

        // test that raw notification is split caused by small output buffer

        cd = CS_ASCII.newDecoder();
        decoder = new DecodeNotifier(cd, 100, 2);

        lst = new TestListenerRW();
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x41, 0x42, 0x43);
        decoder.decode(is);
        assertEquals("[ST][RW]4142[CH]AB[RW]43[CH]C[EN]", lst.toString());

        /*
            test that raw notification with locking-shift is split
            caused by small output buffer
        */

        cs = CS_ISO2022;
        cd = cs.newDecoder();
        decoder = new DecodeNotifier(cd, 100, 2);

        lst = new TestListenerRW();
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(
                0x1b, 0x24, 0x42,  // locking shift to JIS X 0208-1983
                0x30, 0x21,        // '亜' U+4E9C
                0x30, 0x22,        // '唖' U+5516
                0x30, 0x23,        // '娃' U+5A03
                0x1b, 0x28, 0x42   // locking shift to ISO/IEC 646
                );
        decoder.decode(is);
        assertEquals(
                "[ST][RW]1b244230213022[CH]\u4e9c\u5516"
                +"[RW]30231b2842[CH]\u5a03[EN]",
                lst.toString());

        // test that raw notification with decoding error

        cd = CS_ASCII.newDecoder();
        decoder = new DecodeNotifier(cd);

        lst = new TestListenerRW();
        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x41, 0xff, 0x43);
        decoder.decode(is);
        assertEquals("[ST][RW]41[CH]A[ME]ff[RW]43[CH]C[EN]", lst.toString());

        return;
    }

    /**
     * Test of choking method, of class DecodeNotifier.
     * @throws java.lang.Exception
     */
    @Test
    public void testChokingMode() throws Exception {
        System.out.println("choking");

        CharsetDecoder cd;
        DecodeNotifier decoder;

        // test choking mode status switching

        cd = CS_ASCII.newDecoder();
        decoder = new DecodeNotifier(cd);

        assertFalse(decoder.isChokingMode());

        decoder.setChokingMode(true);
        assertTrue(decoder.isChokingMode());

        decoder.setChokingMode(false);
        assertFalse(decoder.isChokingMode());

        return;
    }

    /**
     * Test of choking method, of class DecodeNotifier.
     * @throws java.lang.Exception
     */
    @Test
    public void testChoking() throws Exception {
        System.out.println("choking");

        CharsetDecoder cd;
        DecodeNotifier decoder;
        TestListener lst;
        InputStream is;

        lst = new TestListenerRW();

        // test choking switch with multi-byte encoding

        cd = CS_UTF16BE.newDecoder();
        decoder = new DecodeNotifier(cd);

        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0x00, 0x41, 0x00, 0x42, 0x00, 0x43);
        decoder.decode(is);
        assertEquals("[ST][RW]004100420043[CH]ABC[EN]", lst.toString());

        lst.clear();
        is = byteStream(0x00, 0x41, 0x00, 0x42, 0x00, 0x43);
        decoder.setChokingMode(true);
        decoder.decode(is);
        assertEquals(
                "[ST]"
                +"[RW]0041[CH]A[RW]0042[CH]B[RW]0043[CH]C"
                +"[EN]",
                lst.toString());

        lst.clear();
        is = byteStream(0x00, 0x41, 0x00, 0x42, 0x00, 0x43);
        decoder.setChokingMode(false);
        decoder.decode(is);
        assertEquals("[ST][RW]004100420043[CH]ABC[EN]", lst.toString());

        // test choking with surrogate pair

        cd = CS_UTF16BE.newDecoder();
        decoder = new DecodeNotifier(cd);
        decoder.setChokingMode(true);

        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0xd8, 0x40, 0xdc, 0x00);  // 𠀀 U+20000 with UTF-16BE
        decoder.decode(is);
        assertEquals("[ST][RW]d840dc00[CH]\ud840\udc00[EN]", lst.toString());

        cd = CS_UTF8.newDecoder();
        decoder = new DecodeNotifier(cd);
        decoder.setChokingMode(true);

        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(0xf0, 0xa0, 0x80, 0x80);  // 𠀀 U+20000 with UTF-8
        decoder.decode(is);
        assertEquals("[ST][RW]f0a08080[CH]\ud840\udc00[EN]", lst.toString());

        // test choking with locking-shift

        cd = CS_ISO2022.newDecoder();
        decoder = new DecodeNotifier(cd);
        decoder.setChokingMode(true);

        decoder.setCharDecodeListener(lst);

        lst.clear();
        is = byteStream(
                0x1b, 0x24, 0x42,  // locking shift to JIS X 0208-1983
                0x30, 0x21,        // '亜' U+4E9C
                0x30, 0x22,        // '唖' U+5516
                0x30, 0x23,        // '娃' U+5A03
                0x1b, 0x28, 0x42,  // locking shift to ISO/IEC 646
                0x30, 0x21
                );
        decoder.decode(is);
        assertEquals(
                "[ST][RW]1b2442"
                +"[RW]3021[CH]\u4e9c[RW]3022[CH]\u5516[RW]3023[CH]\u5a03"
                +"[RW]1b2842[RW]30[CH]0[RW]21[CH]![EN]",
                lst.toString());

        return;
    }

}
