/*
 * License : The MIT License
 * Copyright(c) 2018 olyutorskii
 */

package io.bitbucket.olyutorskii.jiocema;

/**
 * Exception when {@link CharDecodeListener} wants to break decoding flow.
 */
@SuppressWarnings("serial")
public class DecodeBreakException extends Exception {

    /**
     * Constructor.
     */
    public DecodeBreakException() {
        super();
        return;
    }

    /**
     * Constructor.
     *
     * @param msg the detail message.
     */
    public DecodeBreakException(String msg) {
        super(msg);
        return;
    }

    /**
     * Constructor.
     *
     * @param cause the cause.
     */
    public DecodeBreakException(Throwable cause) {
        super(cause);
        return;
    }

    /**
     * Constructor.
     *
     * @param msg the detail message.
     * @param cause the cause.
     */
    public DecodeBreakException(String msg, Throwable cause) {
        super(msg, cause);
        return;
    }

}
