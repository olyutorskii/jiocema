/*
 * License : The MIT License
 * Copyright(c) 2018 olyutorskii
 */

package io.bitbucket.olyutorskii.jiocema;

import java.nio.charset.CharsetDecoder;

/**
 * Empty implementation of {@link CharDecodeListener}.
 */
final class EmptyListener implements CharDecodeListener {

    /**
     * Singleton dummy listener.
     */
    static final CharDecodeListener DUMMY_LISTENER = new EmptyListener();


    /**
     * Constructor.
     */
    private EmptyListener() {
        super();
        return;
    }


    /**
     * {@inheritDoc}
     *
     * <p>But, do nothing.
     *
     * @param decoder {@inheritDoc}
     */
    @Override
    public void startDecoding(CharsetDecoder decoder) {
        return;
    }

    /**
     * {@inheritDoc}
     *
     * <p>But, do nothing.
     */
    @Override
    public void endDecoding() {
        return;
    }

    /**
     * {@inheritDoc}
     *
     * <p>But, do nothing.
     *
     * @param byteArray {@inheritDoc}
     * @param offset {@inheritDoc}
     * @param length {@inheritDoc}
     */
    @Override
    public void rawBytes(byte[] byteArray, int offset, int length) {
        return;
    }

    /**
     * {@inheritDoc}
     *
     * <p>But, do nothing.
     *
     * @param charArray {@inheritDoc}
     * @param offset {@inheritDoc}
     * @param length {@inheritDoc}
     */
    @Override
    public void charContent(char[] charArray, int offset, int length) {
        return;
    }

    /**
     * {@inheritDoc}
     *
     * <p>But, do nothing.
     *
     * @param errorArray {@inheritDoc}
     * @param offset {@inheritDoc}
     * @param length {@inheritDoc}
     */
    @Override
    public void malformedError(byte[] errorArray, int offset, int length) {
        return;
    }

    /**
     * {@inheritDoc}
     *
     * <p>But, do nothing.
     *
     * @param errorArray {@inheritDoc}
     * @param offset {@inheritDoc}
     * @param length {@inheritDoc}
     */
    @Override
    public void unmapError(byte[] errorArray, int offset, int length) {
        return;
    }

}
