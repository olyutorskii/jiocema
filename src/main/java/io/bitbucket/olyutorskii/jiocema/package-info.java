/*
 * License : The MIT License
 * Copyright(c) 2018 olyutorskii
 */

/**
 * Byte-sequence to char-sequence decoder
 * with callback listener notification.
 *
 * <p>This package depends on
 * NIO(java.nio.charset) package.
 * This package delegates decoding process to
 * NIO {@link java.nio.charset.CharsetDecoder},
 * but {@link java.nio.ByteBuffer} and {@link java.nio.CharBuffer}
 * operations are hidden.
 *
 * <p>Supported input is {@link java.io.InputStream}.
 *
 * <p>Supported output is
 * {@link io.bitbucket.olyutorskii.jiocema.CharDecodeListener},
 * so you must implement 6-method listener
 * to receive decoding notification.
 */
package io.bitbucket.olyutorskii.jiocema;
