/*
 * License : The MIT License
 * Copyright(c) 2018 olyutorskii
 */

package io.bitbucket.olyutorskii.jiocema;

import java.nio.charset.CharsetDecoder;
import java.util.EventListener;

/**
 * The listener interface for character decoding.
 *
 * <p>Listener receives notification
 * according to character decoding process
 * of {@link DecodeNotifier}.
 *
 * <p>Method call flow at decoding:
 * <ol>
 * <li>{@link #startDecoding} 1-time.
 * <li>({@link #rawBytes }
 * or {@link #charContent}
 * or {@link #unmapError}
 * or {@link #malformedError}) N-times.
 * <li>{@link #endDecoding} 1-time.
 * </ol>
 *
 * <p>Each methods throw {@link DecodeBreakException}
 * if they want to break decoding immediately for some reason.
 */
public interface CharDecodeListener extends EventListener {

    /**
     * Receive notification of the beginning of decoding.
     *
     * <p>Don't touch any property on decoder.
     *
     * @param decoder character decoder
     * @throws DecodeBreakException Throw if you want to break
     *      decoding immediately.
     */
    public abstract void startDecoding(CharsetDecoder decoder)
            throws DecodeBreakException;

    /**
     * Receive notification of the end of decoding.
     *
     * <p>No more notification.
     *
     * @throws DecodeBreakException Throw if you want to break
     *      decoding immediately.
     */
    public abstract void endDecoding()
            throws DecodeBreakException;

    /**
     * Receive notification
     * that raw bytes on input-byte-buffer has been consumed.
     *
     * <p>Error byte sequence is not included.
     *
     * <p>If you want to use byte sequence later,
     * you must copy it before return.
     *
     * @param byteArray byte array containing raw bytes
     * @param offset raw bytes start position
     * @param length raw bytes length
     * @throws DecodeBreakException Throw if you want to break
     *      decoding immediately.
     */
    public abstract void rawBytes(byte[] byteArray, int offset, int length)
            throws DecodeBreakException;

    /**
     * Receive notification of decoded character sequence.
     *
     * <p>If you want to use character sequence later,
     * you must copy it before return.
     *
     * @param charArray char array containing character sequence
     * @param offset character sequence start position
     * @param length character sequence length
     * @throws DecodeBreakException Throw if you want to break
     *      decoding immediately.
     */
    public abstract void charContent(char[] charArray, int offset, int length)
            throws DecodeBreakException;

    /**
     * Receive notification of malformed-sequence error.
     *
     * <p>If you want to use error sequence later,
     * you must copy it before return.
     *
     * @param errorArray byte array containing error sequence
     * @param offset error sequence start position
     * @param length error sequence length
     * @throws DecodeBreakException Throw if you want to break
     *      decoding immediately.
     */
    public abstract void malformedError(byte[] errorArray, int offset, int length)
            throws DecodeBreakException;

    /**
     * Receive notification of character-set unmapping error.
     *
     * <p>If you want to use error sequence later,
     * you must copy it before return.
     *
     * @param errorArray byte array containing error sequence
     * @param offset error sequence start position
     * @param length error sequence length
     * @throws DecodeBreakException Throw if you want to break
     *      decoding immediately.
     */
    public abstract void unmapError(byte[] errorArray, int offset, int length)
            throws DecodeBreakException;

}
