/*
 * License : The MIT License
 * Copyright(c) 2018 olyutorskii
 */

package io.bitbucket.olyutorskii.jiocema;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.text.MessageFormat;
import java.util.Arrays;

/**
 * Byte-sequence to char-sequence decoder.
 *
 * <p>It supports any charset decoding
 * supported by {@link java.nio.charset.CharsetDecoder}(NIO).
 *
 * <p>It decodes char-sequence from input byte stream,
 * and notifies the decoded result and error
 * to listener <code>{@link CharDecodeListener}</code>.
 *
 * <p>This class is designed to hide the complexity of
 * <code>{@link java.nio.charset.CharsetDecoder}</code>,
 * using &quot;Inversion of Control&quot;.
 * {@link java.nio.ByteBuffer} and {@link java.nio.CharBuffer}
 * operations are hidden.
 *
 * <p>This class is designed to replace
 * <code>{@link java.io.InputStreamReader}</code>
 * and
 * <code>
 * {@link String#String(byte[], int, int, java.nio.charset.Charset)}
 * </code>
 * which can not detect details of char decoding error.
 *
 * @see java.nio.charset.CharsetDecoder
 */
public class DecodeNotifier {

    /** Default input buffer size(={@value}bytes)。 */
    public static final int DEFSZ_BYTEBUF = 2 * 1024;
    /** Default output buffer size(={@value}chars)。 */
    public static final int DEFSZ_CHARBUF = 1 * 1024;

    private static final int DEFSZ_ERRBUF = 2;

    private static final String MSGFORM_INFLOOP =
            "too small input buffer ({0}bytes) for {1}";
    private static final String MSGFORM_INBUFLEN =
            "input buffer must have an effective length";
    private static final String MSGFORM_OUTBUFLEN =
            "output buffer length must be 2 or more for surrogate pair";

    private static final InputStream DMY_IS =
            new ByteArrayInputStream(new byte[0]);


    private final CharsetDecoder decoder;
    private InputStream istream;

    private final byte[] byteInData;
    private final char[] charOutData;
    private byte[] errorOutData;

    private final ByteBuffer byteInBuffer;
    private final CharBuffer charOutBuffer;

    private CharDecodeListener listener;

    private boolean isChokingMode;

    private CoderResult decodingResult;
    private boolean hasMoreInput;
    private boolean isFlushing;
    private boolean decodeDone;


    /**
     * Constructor.
     *
     * <p>Buffer size is the default value
     * for both input and output.
     *
     * @param decoder decoder. Error action status will be modified.
     */
    public DecodeNotifier(CharsetDecoder decoder) {
        this(decoder, DEFSZ_BYTEBUF, DEFSZ_CHARBUF);
        return;
    }

    /**
     * Constructor.
     *
     * @param decoder decoder. Error action status will be modified.
     * @param inbufSz input buffer size(bytes).
     * @param outbufSz output buffer size(chars).
     *      It needs at least 2 for surrogate pair.
     * @throws IllegalArgumentException buffer size is too small
     */
    public DecodeNotifier(CharsetDecoder decoder, int inbufSz, int outbufSz )
            throws IllegalArgumentException {
        super();

        if (inbufSz < 1) {
            throw new IllegalArgumentException(MSGFORM_INBUFLEN);
        }

        if (outbufSz < 2) {
            throw new IllegalArgumentException(MSGFORM_OUTBUFLEN);
        }

        this.decoder = decoder;

        this.byteInData = new byte[inbufSz];
        this.charOutData = new char[outbufSz];
        this.errorOutData = new byte[DEFSZ_ERRBUF];

        this.byteInBuffer = ByteBuffer.wrap(this.byteInData);
        this.charOutBuffer = CharBuffer.wrap(this.charOutData);

        this.istream = DMY_IS;
        this.listener = EmptyListener.DUMMY_LISTENER;

        this.isChokingMode = false;

        initDecoderImpl();

        return;
    }


    /**
     * Initialize decoder.
     */
    protected void initDecoder() {
        initDecoderImpl();
        return;
    }

    /**
     * Initialize decoder.
     *
     * <p>This is implementation of {@link #initDecoder()}
     */
    private void initDecoderImpl() {
        this.byteInBuffer.clear().flip();
        this.charOutBuffer.clear();

        this.decoder.onMalformedInput     (CodingErrorAction.REPORT);
        this.decoder.onUnmappableCharacter(CodingErrorAction.REPORT);
        this.decoder.reset();

        this.decodingResult = CoderResult.UNDERFLOW;
        this.hasMoreInput = true;
        this.isFlushing = false;
        this.decodeDone = false;

        Arrays.fill(this.errorOutData, (byte) 0x00);

        return;
    }

    /**
     * Set character decode listener
     * to receive byte to character decoding notification.
     *
     * <p>If listener is null, nothing is notified.
     *
     * @param listenerArg listener
     */
    public void setCharDecodeListener(CharDecodeListener listenerArg) {
        if (listenerArg == null) {
            this.listener = EmptyListener.DUMMY_LISTENER;
        } else {
            this.listener = listenerArg;
        }
        return;
    }

    /**
     * Set choking mode.
     *
     * <p>Choking mode reduces conversion buffering granularity.
     *
     * <p>In choking mode, 1 byte putting is tried each time
     * until one character is converted.
     *
     * <p>Choking mode spends more CPU time
     * and increases notification call.
     *
     * @param isChoking true if choking mode
     * @see #isChokingMode()
     */
    public void setChokingMode(boolean isChoking) {
        this.isChokingMode = isChoking;
        return;
    }

    /**
     * Return whether choking mode or not.
     *
     * @return true if choking mode
     * @see #setChokingMode(boolean)
     */
    public boolean isChokingMode() {
        boolean result = this.isChokingMode;
        return result;
    }

    /**
     * Return input-byte-buffer. (encoded raw byte sequence holder)
     *
     * @return input-byte-buffer
     */
    protected ByteBuffer getByteBuffer() {
        return this.byteInBuffer;
    }

    /**
     * Return output-char-buffer. (decoded char sequence holder)
     *
     * @return output-char-buffer
     */
    protected CharBuffer getCharBuffer() {
        return this.charOutBuffer;
    }

    /**
     * If decode-error is happened, return true.
     *
     * @return true if there is decoding error
     */
    protected boolean hasDecodeError() {
        boolean result = this.decodingResult.isError();
        return result;
    }

    /**
     * If there is no more space in char-output-buffer,
     * return true.
     *
     * @return true if no more space in char buffer
     */
    protected boolean isCharBufOver() {
        boolean result = this.decodingResult.isOverflow();
        return result;
    }

    /**
     * Check whether there is more input data or not.
     *
     * @return true if more input data
     */
    protected boolean hasMoreInput() {
        boolean result = this.hasMoreInput;
        return result;
    }

    /**
     * If flushing done, return true.
     *
     * @return true if flushing done
     */
    protected boolean isFlushDone() {
        boolean result = this.decodeDone;
        return result;
    }

    /**
     * Start character decoding from input stream.
     *
     * <p>Various notifications to the listener are made
     * according to the state of character decoding.
     *
     * <p>When the end of the input stream is reached,
     * the decoding operation ends and input stream is closed.
     *
     * @param istream input stream
     * @throws IOException Input error
     * @throws DecodeBreakException terminated by listener
     */
    public void decode(InputStream istream)
            throws IOException,
                   DecodeBreakException {
        this.istream = istream;

        try {
            decodeImpl();
        } finally {
            try {
                this.istream.close();
            } finally {
                this.istream = DMY_IS;
            }
        }

        return;
    }

    /**
     * Start character decoding from Input stream.
     *
     * <p>This is implementation of {@link #decode(InputStream)}
     *
     * @throws IOException Input error
     * @throws DecodeBreakException terminated by listener
     */
    private void decodeImpl()
            throws IOException,
                   DecodeBreakException {
        initDecoder();

        this.listener.startDecoding(this.decoder);

        do {
            if (isCharBufOver()) {
                sweepOutCharBuf();
            } else if (hasMoreInput()) {
                supplyInputBytes();
            }
            decodeByte2Char();
        } while ( ! isFlushDone() );

        sweepOutCharBuf();

        this.listener.endDecoding();

        return;
    }

    /**
     * Decode from byte buffer to char buffer,
     * and skip error sequence,
     * and notify listener.
     *
     * @throws DecodeBreakException terminated by listener
     * @throws IOException Input error
     */
    private void decodeByte2Char()
            throws DecodeBreakException, IOException {
        int offset = this.byteInBuffer.position();
        int decodedSpan = doDecodeOrFlush();
        if (decodedSpan > 0) {
            notifyRawBytesConsumed(offset, decodedSpan);
            if (isChokingMode()) sweepOutCharBuf();
        }

        int errorSpan = 0;
        if (hasDecodeError()) {
            sweepOutCharBuf();
            this.byteInBuffer.compact().flip();
            errorSpan = skipErrorBytes();
        }

        if (decodedSpan <= 0 && errorSpan <= 0) {
            infLoopCheck();
        }

        return;
    }

    /**
     * Decode or flush with decoder.
     *
     * <p>Progress status flags are managed.
     *
     * @return decoded bytes span length
     */
    private int doDecodeOrFlush() {
        int decodedSpan = 0;

        if ( ! this.isFlushing ) {
            int oldPos = this.byteInBuffer.position();

            boolean endOfInput = ! this.hasMoreInput;
            this.decodingResult = this.decoder.decode(this.byteInBuffer,
                                                      this.charOutBuffer,
                                                      endOfInput);
            if (endOfInput && this.decodingResult.isUnderflow()) {
                this.isFlushing = true;
            }

            int newPos = this.byteInBuffer.position();
            decodedSpan = newPos - oldPos;
        } else {
            this.decodingResult = this.decoder.flush(this.charOutBuffer);
            if (this.decodingResult.isUnderflow()) {
                this.decodeDone = true;
            }
        }

        return decodedSpan;
    }

    /**
     * Infinite loop check.
     *
     * @throws IllegalStateException Detection of infinite loop
     *      due to too small buffer
     */
    private void infLoopCheck() throws IllegalStateException {
        int pos      = this.byteInBuffer.position();
        int limit    = this.byteInBuffer.limit();
        int capacity = this.byteInBuffer.capacity();
        boolean noMoreInputSpace = pos == 0 && limit == capacity;

        boolean deadLock =
                noMoreInputSpace && this.decodingResult.isUnderflow();

        // Detection of infinite loop due to too small input buffer
        if (deadLock) {
            String csName = this.decoder.charset().name();
            String msg = MessageFormat.format(
                    MSGFORM_INFLOOP, capacity, csName);
            throw new IllegalStateException(msg);
        }

        return;
    }

    /**
     * Read encoded bytes from stream into input-byte-buffer.
     *
     * <p>Bytes left in the previous decoding process
     * are refilled at the beginning of input-byte-buffer. (compaction)
     *
     * <p>It is necessary to prepare free space
     * in input-byte-buffer before.
     *
     * <p>If no more input, do nothing.
     *
     * <p>If choking-mode, read 1byte only each time.
     *
     * @return input byte length.
     *      It returns a negative value
     *      when it reaches the input termination.
     *      Value 0 is impossible.
     * @throws IOException I/O error
     */
    protected int supplyInputBytes()
            throws IOException {
        if ( ! hasMoreInput() ) {
            return -1;
        }

        this.byteInBuffer.compact();

        int ioSz;
        if (isChokingMode()) {
            ioSz = 1;
        } else {
            ioSz = this.byteInBuffer.remaining();
        }
        assert ioSz > 0;

        // similar to reading from blocking-mode channnel to buffer
        int offset = this.byteInBuffer.position();
        int ioDone = this.istream.read(this.byteInData, offset, ioSz);
        assert ioSz != 0 && ioDone != 0;

        if (ioDone > 0) {
            int newPos = offset + ioDone;
            this.byteInBuffer.position(newPos);
        } else {
            assert ioDone < 0;
            this.hasMoreInput = false;
        }

        this.byteInBuffer.flip();

        return ioDone;
    }

    /**
     * Sweep out output-char-buffer
     * and notify decode listener.
     *
     * <p>Output-char-buffer will be cleared.
     *
     * <p>If output-char-buffer is already empty, do nothing.
     *
     * @return number of chars swept
     * @throws DecodeBreakException terminated by listener
     */
    protected int sweepOutCharBuf() throws DecodeBreakException {
        int lastPos = this.charOutBuffer.position();
        if (lastPos <= 0) return 0;

        final int offset = 0;
        this.listener.charContent(this.charOutData, offset, lastPos);

        this.charOutBuffer.clear();

        return lastPos;
    }

    /**
     * Notify that raw byte on input-byte-buffer has been consumed.
     *
     * <p>Error sequence is not included.
     *
     * @param offset offset in buffer
     * @param length raw bytes length
     * @throws DecodeBreakException terminated by listener
     */
    private void notifyRawBytesConsumed(int offset, int length)
            throws DecodeBreakException {
        this.listener.rawBytes(this.byteInData, offset, length);
        return;
    }

    /**
     * Skip error bytes from input-byte-buffer.
     *
     * <p>Position of input-byte-buffer advances by the length of error.
     *
     * @return skipped error length
     * @throws DecodeBreakException terminated by listener
     * @throws IOException I/O error
     */
    private int skipErrorBytes()
            throws DecodeBreakException, IOException {
        assert hasDecodeError();
        this.decodingResult = modifyErrorResult(this.decodingResult);
        assert this.decodingResult.isError();

        int errorLength = this.decodingResult.length();
        assert errorLength > 0;
        reassignErrorArray(errorLength);

        final int offset = 0;
        this.byteInBuffer.get(this.errorOutData, offset, errorLength);

        notifyErrorBytesSkipped(offset, errorLength);

        return errorLength;
    }

    /**
     * Modify error bytes sequence.
     *
     * <p>It was prepared for irregular encoding specific
     * to each character encoding.
     * The default implementation simply returns the argument.
     *
     * <p>By shrinking the error bytes forward,
     * the rear part of the byte sequence
     * (that was supposed to be treated as an error)
     * is handled again as a target of decoding process.
     *
     * <p>It is also possible to exchange
     * malformed-error and unmappable-error
     *
     * <p>If necessary, you can rebuild longer error information
     * by reading look-ahead I/O using {@link #supplyInputBytes()}.
     *
     * @param errInfo error info before modifying.
     * @return modified error info.
     *      If nothing modified, errInfo will return.
     * @throws IOException I/O error if look-ahead I/O failed.
     */
    protected CoderResult modifyErrorResult(CoderResult errInfo)
            throws IOException {
        CoderResult modified = errInfo;
        return modified;
    }

    /**
     * Reassign error-bytes-array.
     *
     * <p>Old data is retained.
     * Arrays never shrink.
     *
     * <p>If new size is smaller, nothing happen.
     *
     * @param newSize new size of error-bytes-array.
     */
    private void reassignErrorArray(int newSize) {
        int oldLength = this.errorOutData.length;
        if (oldLength >= newSize) return;

        int extSize = Math.max(newSize, oldLength * 2);

        byte[] newArray = Arrays.copyOf(this.errorOutData, extSize);
        this.errorOutData = newArray;

        return;
    }

    /**
     * Notify that error bytes on input-byte-buffer have been skipped.
     *
     * @param offset offset in error-bytes-array
     * @param length raw bytes length
     * @throws DecodeBreakException terminated by listener
     */
    private void notifyErrorBytesSkipped(int offset, int length)
            throws DecodeBreakException {
        if (this.decodingResult.isUnmappable()) {
            this.listener.unmapError(this.errorOutData, offset, length);
        } else if (this.decodingResult.isMalformed()) {
            this.listener.malformedError(this.errorOutData, offset, length);
        } else {
            assert false;
        }

        return;
    }

}
