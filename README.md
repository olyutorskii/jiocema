# JioCema #

-----------------------------------------------------------------------


## What is JioCema ? ##

* **JioCema** is a Java library
for **decoding character-sequence from byte-sequence**.
It is using **Inversion of Control**(a.k.a.Callback) programming model.

* Since this library depends on Java NIO classes,
any character decodings supported by NIO runtime can be decoded.

* This library is designed to hide the complexity of
`java.nio.charset.CharsetDecoder`.
Therefore, `java.nio.ByteBuffer`and `java.nio.CharBuffer`
operations are hidden.

* This library is designed to replace
`java.io.InputStreamReader` and
`String#String(byte[], int, int, Charset)`
which can not detect details of char decoding error.


## How to use library ##

* You need listener instance that implements `CharDecodeListener` interface.

* You need `InputStream` instance to input byte-sequence.

* Let's construct `DecodeNotifier` instance with `CharsetDecoder`.

* If you set a listener on `DecodeNotifier` instance
and pass `InputStream` instance to the `decode()` method,
the listener will be notified of the decoding process result.


## How to build ##

* JioCema needs to use [Maven 3.3.9+](https://maven.apache.org/)
and JDK 1.8+ to be built.

* JioCema runtime does not depend on any other library at all.
Just compile Java sources under `src/main/java/` if you don't use Maven.


## License ##

* Code is under [The MIT License](https://opensource.org/licenses/MIT).


## Project founder ##

* By [olyutorskii](https://bitbucket.org/olyutorskii/) at 2018


## Key technology ##

- [CharsetDecoder (Javadoc API)](https://docs.oracle.com/javase/7/docs/api/java/nio/charset/CharsetDecoder.html)
- [Inversion of control (Wikipedia)](https://en.wikipedia.org/wiki/Inversion_of_control)

--- EOF ---
